syntax keyword labKeyword ADD SUB MUL DIV
syntax keyword labKeyword AND NAN ORR NOR
syntax keyword labKeyword XOR MOD BGT BEQ
syntax keyword labKeyword LDR STR GET PUT

syntax match labLabel /\v\w+:/
syntax match labRegister /\v(\W|^)(r|R)\d{1,3}[^\t ;]*/
syntax match labRegister /\v(\W|^)(o|O)\o{1,3}[^\t ;]*/
syntax match labRegister /\v(\W|^)(x|X)\x{1,3}[^\t ;]*/
syntax match labNumber /\v(\W|^)\d+/
syntax match labNumber /\v(\W|^)0\o+/
syntax match labNumber /\v(\W|^)0(x|X)\x+/
syntax match labComment /\v;.*$/

syntax region labString start=/\v"/ skip=/\v\\./ end=/\v"/

highlight link labKeyword Function
highlight link labComment Comment
highlight link labLabel PreProc
highlight link labString String
highlight link labRegister Operator
highlight link labNumber Number

set comments+=:;
syntax case ignore
