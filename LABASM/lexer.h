#ifndef LABASM_LEXER_H
#define LABASM_LEXER_H

#include <stdio.h>

typedef enum LexemeType {
    LT_STRING = 's',
    LT_OVERFLOW = '|',
    LT_NUMBER = 'n',
    LT_COLON = ':',
    LT_IDENTIFIER = 'i'
} LexemeType;

typedef struct Lexeme {
    LexemeType type;
    char *contents; /* Dynamically allocated. */
} Lexeme;

typedef struct LexemeList {
    Lexeme *lexemes;
    int lexeme_count;
} LexemeList;

LexemeList lex_file(FILE *);

#endif
