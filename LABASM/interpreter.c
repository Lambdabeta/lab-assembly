#include "interpreter.h"
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

static int o_atoi(const char *str)
{
    int i;
    int ret=0;
    
    for (i=0; str[i] && isdigit(str[i]) && str[i] != '8' && str[i] != '9'; ++i) {
        ret *= 8;
        ret += str[i] - '0';
    }
    return ret;
}

static int x_atoi(const char *str)
{
    int i;
    int ret=0;
    for (i=0; isxdigit(str[i]); ++i) {
        ret *= 16;
        if (isdigit(str[i])) ret += str[i] - '0';
        else if (islower(str[i])) ret += 10 + str[i] - 'a';
        else ret += 10 + str[i] - 'A';
    }
    return ret;
}
#define SHAMT_ERROR -1
static int getInstructionShamt(const char *str)
{
    int len = strlen(str);
    if (len < 3 || len > 6 || len == 4) return SHAMT_ERROR;
    if (strlen(str) == 3) return 0;
    if (str[3] == '>') {
        switch (str[4]) {
            case '>':
                if (len == 5) return SHAMT_ERROR;
                switch (str[5]) {
                    case '0': return 0;
                    case '1': return 9;
                    case '2': return 10;
                    case '3': return 11;
                    default: return SHAMT_ERROR;
                }
            case '0': return 0;
            case '1': return 1;
            case '2': return 2;
            case '3': return 3;
            default: return SHAMT_ERROR;
        }
    } else if (str[3] == '<') {
        switch (str[4]) {
            case '<':
                if (len == 5) return SHAMT_ERROR;
                switch (str[5]) {
                    case '0': return 0;
                    case '1': return 15;
                    case '2': return 14;
                    case '3': return 13;
                    case '4': return 12;
                    default: return SHAMT_ERROR;
                }
            case '0': return 0;
            case '1': return 7;
            case '2': return 6;
            case '3': return 5;
            case '4': return 4;
            default: return SHAMT_ERROR;
        }
    } else {
        return SHAMT_ERROR;
    }
}

static InstructionName getInstructionName(const char *str)
{
    if (strlen(str) != 3) {
        if (getInstructionShamt(str) == SHAMT_ERROR)
            return IN_ERR;
    }
    if (tolower(str[0]) == 'a' &&
        tolower(str[1]) == 'd' &&
        tolower(str[2]) == 'd') return IN_ADD;
    if (tolower(str[0]) == 's' &&
        tolower(str[1]) == 'u' &&
        tolower(str[2]) == 'b') return IN_SUB;
    if (tolower(str[0]) == 'm' &&
        tolower(str[1]) == 'u' &&
        tolower(str[2]) == 'l') return IN_MUL;
    if (tolower(str[0]) == 'd' &&
        tolower(str[1]) == 'i' &&
        tolower(str[2]) == 'v') return IN_DIV;
    if (tolower(str[0]) == 'a' &&
        tolower(str[1]) == 'n' &&
        tolower(str[2]) == 'd') return IN_AND;
    if (tolower(str[0]) == 'n' &&
        tolower(str[1]) == 'a' &&
        tolower(str[2]) == 'n') return IN_NAN;
    if (tolower(str[0]) == 'o' &&
        tolower(str[1]) == 'r' &&
        tolower(str[2]) == 'r') return IN_ORR;
    if (tolower(str[0]) == 'n' &&
        tolower(str[1]) == 'o' &&
        tolower(str[2]) == 'r') return IN_NOR;
    if (tolower(str[0]) == 'x' &&
        tolower(str[1]) == 'o' &&
        tolower(str[2]) == 'r') return IN_XOR;
    if (tolower(str[0]) == 'm' &&
        tolower(str[1]) == 'o' &&
        tolower(str[2]) == 'd') return IN_MOD;
    if (tolower(str[0]) == 'b' &&
        tolower(str[1]) == 'g' &&
        tolower(str[2]) == 't') return IN_BGT;
    if (tolower(str[0]) == 'b' &&
        tolower(str[1]) == 'e' &&
        tolower(str[2]) == 'q') return IN_BEQ;
    if (tolower(str[0]) == 'l' &&
        tolower(str[1]) == 'd' &&
        tolower(str[2]) == 'r') return IN_LDR;
    if (tolower(str[0]) == 's' &&
        tolower(str[1]) == 't' &&
        tolower(str[2]) == 'r') return IN_STR;
    if (tolower(str[0]) == 'g' &&
        tolower(str[1]) == 'e' &&
        tolower(str[2]) == 't') return IN_GET;
    if (tolower(str[0]) == 'p' &&
        tolower(str[1]) == 'u' &&
        tolower(str[2]) == 't') return IN_PUT;
    return IN_ERR;
}

static int getRegisterNumber(const char *str)
{
    int d = atoi(str+1);
    int x = x_atoi(str+1);
    int o = o_atoi(str+1);
    switch (str[0]) {
        case 'r':
        case 'R':
            if (d >= 0 && d <= 256) return d;
            return -1;
        case 'x':
        case 'X':
            if (x >= 0 && x <= 256) return x;
            return -1;
        case 'o':
        case 'O':
            if (o >= 0 && o <= 256) return o;
            return -1;
        default:
            return -1;
    }
}

SymbolList interpret_lexemes(LexemeList ll)
{
    int i;
    int index=0; /* next spot in ret. */
    SymbolList ret;
    char *swp;
    ret.symbol_count = ll.lexeme_count;
    ret.symbols = malloc(sizeof(Symbol)*(ll.lexeme_count+1));
    
    for (i=0; i<ll.lexeme_count; ++i) {
        switch (ll.lexemes[i].type) {
            case LT_STRING:
                ret.symbols[index].type = ST_STRING;
                ret.symbols[index++].string = ll.lexemes[i].contents;
                break;
            case LT_OVERFLOW:
                /* Fold the lexeme into the next one for later processing. */
                swp = malloc(strlen(ll.lexemes[i].contents)+
                             strlen(ll.lexemes[i+1].contents)+1);
                strcpy(swp,ll.lexemes[i].contents);
                strcat(swp,ll.lexemes[i+1].contents);
                free(ll.lexemes[i].contents);
                free(ll.lexemes[i+1].contents);
                ll.lexemes[i+1].contents = swp;
                ret.symbol_count--;
                break;
            case LT_NUMBER:
                ret.symbols[index].type = ST_NUMBER;
                if (ll.lexemes[i].contents[0] != '0')
                    ret.symbols[index++].number = atoi(ll.lexemes[i].contents);
                else if (!ll.lexemes[i].contents[1] || 
                        isdigit(ll.lexemes[i].contents[1]))
                    ret.symbols[index++].number = o_atoi(ll.lexemes[i].contents);
                else 
                    ret.symbols[index++].number = x_atoi(ll.lexemes[i].contents+2);

                free(ll.lexemes[i].contents);
                break;
            case LT_COLON:
                if (index > 0)
                    ret.symbols[index-1].type = ST_LABEL;
                free(ll.lexemes[i].contents);
                ret.symbol_count--;
                break;
            case LT_IDENTIFIER:
                if (getInstructionName(ll.lexemes[i].contents) != IN_ERR) {
                    ret.symbols[index].type = ST_INSTRUCTION;
                    ret.symbols[index].name = getInstructionName(ll.lexemes[i].contents);
                    ret.symbols[index++].shamt = getInstructionShamt(ll.lexemes[i].contents);
                    free(ll.lexemes[i].contents);
                } else if (getRegisterNumber(ll.lexemes[i].contents) >= 0) {
                    ret.symbols[index].type = ST_REGISTER;
                    ret.symbols[index++].number = getRegisterNumber(ll.lexemes[i].contents);
                    free(ll.lexemes[i].contents);
                } else {
                    ret.symbols[index].type = ST_IDENTIFIER;
                    ret.symbols[index++].string = ll.lexemes[i].contents;
                }
                break;
        }
    }

    free(ll.lexemes);
    return ret;
}
