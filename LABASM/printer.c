#include "printer.h"
#include <stdio.h>
#include <string.h>

void write_int(FILE *file, int i)
{
    unsigned char a = i>>24;
    unsigned char b = (i>>16)&0xFF;
    unsigned char c = (i>>8)&0xFF;
    unsigned char d = i&0xFF;
    fwrite(&a,1,1,file);
    fwrite(&b,1,1,file);
    fwrite(&c,1,1,file);
    fwrite(&d,1,1,file);
}


void write_memory(MemoryProfile mp, FILE *file)
{
    int i,ii;
    for (i=0; i<mp.size; ++i) {
        switch (mp.cells[i].type) {
            case CT_INSTRUCTION:
                write_int(file,
                          (mp.cells[i].shamt) << 28 |
                          (mp.cells[i].name << 24) |
                          (mp.cells[i].r1 << 16) |
                          (mp.cells[i].r2 << 8) | mp.cells[i].r3);
                break;
            case CT_NUMBER:
                write_int(file,mp.cells[i].val);
                break;
            case CT_REFERENCE:
                for (ii=0; ii<mp.size; ++ii) {
                    LabelList *save = mp.cells[ii].labels;
                    while (mp.cells[ii].labels) {
                        if (!strcmp(mp.cells[i].label,mp.cells[ii].labels->string)) {
                            write_int(file,ii);
                            mp.cells[ii].labels = save;
                            goto success;
                        }
                        mp.cells[ii].labels = mp.cells[ii].labels->next;
                    }
                    mp.cells[ii].labels = save;
                }
                printf("Undefined reference to %s!\n",mp.cells[i].label);
success:        break;
            case CT_CHAR:
                write_int(file,mp.cells[i].ch);
                break;
        }
    }
}
