#include "lexer.h"
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

/* MAX 512 chars per lexeme. */
#define BUFFER_SIZE 512

typedef struct LexemeStackElement {
    Lexeme val;
    struct LexemeStackElement *next;
} LexemeStackElement;

typedef struct LexemeStack {
    LexemeStackElement *head;
    int size;
} LexemeStack;

static LexemeStack lexeme_stack_new(void)
{
    LexemeStack ret;
    ret.head = NULL;
    ret.size = 0;
    return ret;
}

static void lexeme_stack_push(LexemeStack *this, LexemeType type, const char *contents)
{
    LexemeStackElement *new = malloc(sizeof(LexemeStackElement));
    new->val.type = type;
    new->val.contents = malloc(sizeof(char)*(strlen(contents)+1));
    strcpy(new->val.contents,contents);
    new->next = this->head;
    this->head = new;
    this->size++;
}


static LexemeStack lex_file_stack(FILE *file)
{
    typedef enum LexerState {
        LEXER_STATE_NONE,
        LEXER_STATE_STRING,
        LEXER_STATE_COMMENT,
        LEXER_STATE_NUMBER,
        LEXER_STATE_IDENTIFIER,
        LEXER_STATE_ESCAPE
    } LexerState;
#define PUSH(X) do { buf[i++] = 0; lexeme_stack_push(&stack,(X),buf); i = 0; } while (0)
#define SAVE do { if (i == BUFFER_SIZE-1) PUSH(LT_OVERFLOW); buf[i++] = ch; } while (0)
    LexemeStack stack = lexeme_stack_new();
    char ch;
    char buf[BUFFER_SIZE];
    int i=0;
    LexerState state = LEXER_STATE_NONE;
    while (!feof(file)) {
        ch = fgetc(file);
        switch (state) {
            case LEXER_STATE_NONE:
                if (ch == '"') {
                    state = LEXER_STATE_STRING;
                } else if (ch == ';') {
                    state = LEXER_STATE_COMMENT;
                } else if (ch == ':') {
                    SAVE;
                    PUSH(LT_COLON);
                } else if (isdigit(ch)) {
                    state = LEXER_STATE_NUMBER;
                    SAVE;
                } else if (isalpha(ch)) { 
                    state = LEXER_STATE_IDENTIFIER;
                    SAVE;
                }
                break;
            case LEXER_STATE_STRING:
                if (ch == '"') {
                    state = LEXER_STATE_NONE;
                    PUSH(LT_STRING);
                } else if (ch == '\\') {
                    state = LEXER_STATE_ESCAPE;
                    SAVE;
                } else {
                    SAVE;
                }
                break;
            case LEXER_STATE_ESCAPE:
                state = LEXER_STATE_STRING;
                SAVE;
                break;
            case LEXER_STATE_COMMENT:
                if (ch == '\n') state = LEXER_STATE_NONE;
                break;
            case LEXER_STATE_NUMBER:
                if (ch == ';') {
                    state = LEXER_STATE_COMMENT;
                    PUSH(LT_NUMBER);
                    break;
                }
                if (isspace(ch)) {
                    PUSH(LT_NUMBER);
                    state = LEXER_STATE_NONE;
                } 
                if (!isspace(ch)) SAVE;
                break;
            case LEXER_STATE_IDENTIFIER:
                if (ch == ';') {
                    state = LEXER_STATE_COMMENT;
                    PUSH(LT_IDENTIFIER);
                    break;
                }
                if (isspace(ch)||ch==':') {
                    PUSH(LT_IDENTIFIER);
                    state = LEXER_STATE_NONE;
                }
                if (!isspace(ch)) SAVE;
                if (ch == ':') PUSH(LT_COLON);
                break;
        }
    }
    return stack;
#undef PUSH
#undef SAVE
}
        
LexemeList lex_file(FILE *file)
{
    LexemeList ret;
    LexemeStack stack;
    int i;

    stack = lex_file_stack(file);
    
    ret.lexeme_count = stack.size;
    ret.lexemes = malloc(sizeof(Lexeme)*stack.size);

    for (i=0; i<stack.size; ++i) {
        LexemeStackElement *tmp;
        ret.lexemes[stack.size - (i+1)] = stack.head->val;
        tmp = stack.head;
        stack.head = stack.head->next;
        free(tmp);
    }

    return ret;
}

