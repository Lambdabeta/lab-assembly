#ifndef LABASM_EXPANDER_H
#define LABASM_EXPANDER_H

#include "parser.h"

typedef enum CellType {
    CT_INSTRUCTION,
    CT_NUMBER,
    CT_REFERENCE,
    CT_CHAR
}CellType;

typedef struct MemoryCell {
    CellType type;
    union {
        struct {
            InstructionName name;
            int shamt;
            int r1,r2,r3;
        };
        int val;
        char *label;
        char ch;
    };
    LabelList *labels; /* Same structure... for now. */
} MemoryCell;

typedef struct MemoryProfile {
    MemoryCell *cells;
    int size;
    /* TODO: add a proper symbol table. */
} MemoryProfile;

MemoryProfile expand_memory(MemoryList);

#endif /* LABASM_EXPANDER_H */
