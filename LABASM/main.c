#include "lexer.h"
#include "interpreter.h"
#include "parser.h"
#include "expander.h"
#include "printer.h"
#include <stdio.h>
#include <stdlib.h>

void print_cell(MemoryCell cell)
{
    LabelList *save;
    /* First print all labels */
    save = cell.labels;
    while (cell.labels) {
        printf("%s: ", cell.labels->string);
        cell.labels = cell.labels->next;
    }
    cell.labels = save;
    switch (cell.type) {
        case CT_INSTRUCTION:
            printf("%d%%%d r%d r%d r%d\n",cell.shamt,cell.name,cell.r1,cell.r2,cell.r3);
            break;
        case CT_NUMBER:
            printf("%d\n",cell.val);
            break;
        case CT_REFERENCE:
            printf("%s\n",cell.label);
            break;
        case CT_CHAR:
            printf("\'%c\'\n",cell.ch);
            break;
    }
}

void print_memory(Memory mem)
{
    LabelList *save;
    if (mem.type == MT_EOF) return;
    /* First print all labels */
    save = mem.labels;
    while (mem.labels) {
        printf("%s: ", mem.labels->string);
        mem.labels = mem.labels->next;
    }
    mem.labels = save;

    switch (mem.type) {
        case MT_NUMBER:
            printf("%d\n", mem.number);
            break;
        case MT_STRING:
            printf("\"%s\"\n", mem.string);
            break;
        case MT_INSTRUCTION:
            printf("%d%%%d ", mem.shamt, mem.name);
            switch (mem.r1.type) {
                case RT_NORMAL: printf("r%d ",mem.r1.number); break;
                case RT_LITERAL: printf("%d ",mem.r1.number); break;
                case RT_LABEL: printf("%s ",mem.r1.string); break;
            }
            switch (mem.r2.type) {
                case RT_NORMAL: printf("r%d ",mem.r2.number); break;
                case RT_LITERAL: printf("%d ",mem.r2.number); break;
                case RT_LABEL: printf("%s ",mem.r2.string); break;
            }
            switch (mem.r3.type) {
                case RT_NORMAL: printf("r%d ",mem.r3.number); break;
                case RT_LITERAL: printf("%d ",mem.r3.number); break;
                case RT_LABEL: printf("%s ",mem.r3.string); break;
            }
            break;
    }
}

int main(int argc, char *argv[])
{
    LexemeList llist;
    SymbolList slist;
    MemoryList mlist;
    MemoryProfile memp;
    FILE *file;
    int i,ii,x;
    int ifiles[64]={0};/*input files in terms of argv*/
    char *outfile = "a.out";
    int lex=0;
    int sym=0;
    int par=0;
    int mem=0;
    ii=0;
    for (i=1; i<argc; ++i) {
        if (argv[i][0] == '-') {
            switch (argv[i][1]) {
                case 'l': lex=1; break;
                case 's': sym=1; break;
                case 'p': par=1; break;
                case 'm': mem=1; break;
                case 'o': outfile = argv[++i]; break;
                case 'h': printf(
"LABASM INFILE+ -o OUTFILE [-lspmh]\n\tl - show lexing info\n\ts - show symbolic info\n\tp - show parsing info\n\tm - show memory map\n");
                break;
            }
        } else {
            ifiles[ii++] = i;
        }
    }
    
    /* recreate output file if it exists */
    file = fopen(outfile,"wb");
    fclose(file);

    /* loop over each input file */
    for (i=0; i<ii; ++i) {
        file = fopen(argv[ifiles[i]],"r");
        if (!file) continue;
        llist = lex_file(file);
        fclose(file);

        if (lex) {
            for (x=0; x<llist.lexeme_count; ++x)
                printf("%c - %s\n", llist.lexemes[x].type,llist.lexemes[x].contents);
            printf("\n\n");
        }

        slist = interpret_lexemes(llist);

        if (sym) {
            for (x=0; x<slist.symbol_count; ++x) {
                switch (slist.symbols[x].type) {
                    case ST_STRING:
                        printf("\"%s\" ",slist.symbols[x].string);
                        break;
                    case ST_NUMBER:
                        printf("%d ",slist.symbols[x].number);
                        break;
                    case ST_LABEL:
                        printf("%s: ",slist.symbols[x].string);
                        break;
                    case ST_INSTRUCTION:
                        printf("%d%%%d ",slist.symbols[x].shamt,slist.symbols[x].name);
                        break;
                    case ST_REGISTER:
                        printf("r%d ",slist.symbols[x].number);
                        break;
                    case ST_IDENTIFIER:
                        printf("%s ",slist.symbols[x].string);
                        break;
                }
            }
            printf("\n\n");
        }

        mlist = parse_symbols(slist);

        if (par) {
            for (x=0; mlist.memory[x].type != MT_EOF; ++x)
                print_memory(mlist.memory[x]);
            printf("\n\n");
        }

        memp = expand_memory(mlist);

        if (mem) {
            for (x=0; x<memp.size; ++x)
                print_cell(memp.cells[x]);
            printf("\n\n");
        }

        file = fopen(outfile,"ab");

        write_memory(memp,file);

        fclose(file);

        free(memp.cells);
    }

    return 0;
}

