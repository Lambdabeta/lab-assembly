#ifndef LABASM_INTERPRETER_H
#define LABASM_INTERPRETER_H

#include "lexer.h"

typedef enum SymbolType {
    ST_STRING,
    ST_NUMBER,
    ST_LABEL,
    ST_INSTRUCTION,
    ST_REGISTER,
    ST_IDENTIFIER
} SymbolType;

typedef enum InstructionName {
    IN_ADD = 0,
    IN_SUB,
    IN_MUL,
    IN_DIV,
    IN_AND,
    IN_NAN,
    IN_ORR,
    IN_NOR,
    IN_XOR,
    IN_MOD,
    IN_BGT,
    IN_BEQ,
    IN_LDR,
    IN_STR,
    IN_GET,
    IN_PUT,
    IN_ERR
} InstructionName;

typedef struct Symbol {
    SymbolType type;
    union {
        char *string;
        int number;
        struct {
            InstructionName name;
            int shamt;/*shamt is in terms of nibble representation*/
        };
    };
} Symbol;

typedef struct SymbolList {
    Symbol *symbols;
    int symbol_count;
} SymbolList;

/* NOTE: frees the list given to it. */
SymbolList interpret_lexemes(LexemeList);

#endif /* LABASM_INTERPRETER_H */
