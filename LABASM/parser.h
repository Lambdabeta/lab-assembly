#ifndef LABASM_PARSER_H
#define LABASM_PARSER_H

#include "interpreter.h"

typedef enum RegisterType {
    RT_NORMAL,
    RT_LITERAL,
    RT_LABEL
} RegisterType;

typedef enum MemoryType {
    MT_INSTRUCTION,
    MT_NUMBER,
    MT_STRING,
    MT_EOF
} MemoryType;

typedef struct LabelList {
    char *string;
    struct LabelList *next;
} LabelList;

typedef struct Memory {
    MemoryType type;
    union {
        struct {
            InstructionName name;
            int shamt;
            struct {
                RegisterType type;
                union {
                    int number;
                    char *string;
                };
            }r1,r2,r3;
        };
        int number;
        char *string;
    };
    LabelList *labels; /* if null, no labels, otherwise points to the labels. */
}Memory;
            
typedef struct MemoryList {
    Memory *memory;
    int memory_size;
} MemoryList;

/* NOTE: frees the list given to it. */
MemoryList parse_symbols(SymbolList);

#endif /* LABASM_PARSER_H */
