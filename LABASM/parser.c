#include "parser.h"
#include <stdio.h>
#include <stdlib.h>

/* Used in the recursive descent parser. */
typedef struct ParseState {
    MemoryList *list;
    int i; /* Index on list. */
    SymbolList symbols;
    int index; /* Index on symbols. */
} ParseState;

static void push_label(ParseState *this, char *str)
{
    LabelList *new;
    if (!this->list->memory[this->i].labels) {
        this->list->memory[this->i].labels = malloc(sizeof(LabelList));
        this->list->memory[this->i].labels->string = str;
        this->list->memory[this->i].labels->next = NULL;
        return;
    }
    new = malloc(sizeof(LabelList));
    new->string = str;
    new->next = this->list->memory[this->i].labels;
    this->list->memory[this->i].labels = new;
}

static void parse_registers(ParseState *this)
{
    switch (this->symbols.symbols[this->index].type) {
        case ST_NUMBER:
            this->list->memory[this->i].r1.type = RT_LITERAL;
            this->list->memory[this->i].r1.number = 
                this->symbols.symbols[this->index++].number;
            if (this->list->memory[this->i].r1.number==0)
                this->list->memory[this->i].r1.type = RT_NORMAL;
            break;
        case ST_REGISTER:
            this->list->memory[this->i].r1.type = RT_NORMAL;
            this->list->memory[this->i].r1.number = 
                this->symbols.symbols[this->index++].number;
            break;
        case ST_IDENTIFIER:
            this->list->memory[this->i].r1.type = RT_LABEL;
            this->list->memory[this->i].r1.string = 
                this->symbols.symbols[this->index++].string;
            break;
        default:
            this->index++;
    }
    switch (this->symbols.symbols[this->index].type) {
        case ST_NUMBER:
            this->list->memory[this->i].r2.type = RT_LITERAL;
            this->list->memory[this->i].r2.number = 
                this->symbols.symbols[this->index++].number;
            if (this->list->memory[this->i].r2.number==0)
                this->list->memory[this->i].r2.type = RT_NORMAL;
            break;
        case ST_REGISTER:
            this->list->memory[this->i].r2.type = RT_NORMAL;
            this->list->memory[this->i].r2.number = 
                this->symbols.symbols[this->index++].number;
            break;
        case ST_IDENTIFIER:
            this->list->memory[this->i].r2.type = RT_LABEL;
            this->list->memory[this->i].r2.string = 
                this->symbols.symbols[this->index++].string;
            break;
        default:
            this->index++;
    }
    switch (this->symbols.symbols[this->index].type) {
        case ST_NUMBER:
            this->list->memory[this->i].r3.type = RT_LITERAL;
            this->list->memory[this->i].r3.number = 
                this->symbols.symbols[this->index++].number;
            if (this->list->memory[this->i].r3.number==0)
                this->list->memory[this->i].r3.type = RT_NORMAL;
            break;
        case ST_REGISTER:
            this->list->memory[this->i].r3.type = RT_NORMAL;
            this->list->memory[this->i].r3.number = 
                this->symbols.symbols[this->index++].number;
            break;
        case ST_IDENTIFIER:
            this->list->memory[this->i].r3.type = RT_LABEL;
            this->list->memory[this->i].r3.string = 
                this->symbols.symbols[this->index++].string;
            break;
        default:
            this->index++;
    }
    this->i++;
}

static void parse_memory(ParseState *this)
{
    while (this->symbols.symbols[this->index].type == ST_LABEL)
        push_label(this,this->symbols.symbols[this->index++].string);

    switch (this->symbols.symbols[this->index].type) {
        case ST_STRING:
            this->list->memory[this->i].type = MT_STRING;
            this->list->memory[this->i++].string = 
                this->symbols.symbols[this->index++].string;
            break;
        case ST_NUMBER:
            this->list->memory[this->i].type = MT_NUMBER;
            this->list->memory[this->i++].number = 
                this->symbols.symbols[this->index++].number;
            break;
        case ST_INSTRUCTION:
            this->list->memory[this->i].type = MT_INSTRUCTION;
            this->list->memory[this->i].name = 
                this->symbols.symbols[this->index].name;
            this->list->memory[this->i].shamt = 
                this->symbols.symbols[this->index++].shamt;
            parse_registers(this);
            break;
        default:
            this->index++;
    }
}



static void parse_program(ParseState *this)
{
    while (this->index < this->symbols.symbol_count)
        parse_memory(this);
    this->list->memory[this->i].type = MT_EOF;
}


MemoryList parse_symbols(SymbolList list)
{
    ParseState ps;
    MemoryList ret;
    int i;
    ret.memory_size = list.symbol_count; /* Guaranteed to be at least big enough. */
    ret.memory = malloc(sizeof(Memory) * ret.memory_size);
    for (i=0; i<ret.memory_size; ++i)
        ret.memory[i].labels = NULL;

    ps.list = &ret;
    ps.i = 0;
    ps.symbols = list;
    ps.index = 0;

    parse_program(&ps);

    free(list.symbols);
    return ret;
}
