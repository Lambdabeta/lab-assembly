#include "expander.h"
#include <string.h>
#include <stdlib.h>

MemoryProfile expand_memory(MemoryList list)
{
    int i,ii;
    int index; /* Index on cells */
    MemoryProfile ret;

    /* Step 1: calculate size. */
    ret.size = 0;
    for (i=0; list.memory[i].type != MT_EOF; ++i) {
        switch (list.memory[i].type) {
            case MT_INSTRUCTION: 
                ret.size++; 
                if (list.memory[i].r1.type != RT_NORMAL) ret.size++;
                if (list.memory[i].r2.type != RT_NORMAL) ret.size++;
                if (list.memory[i].r3.type != RT_NORMAL) ret.size++;
                break;
            case MT_NUMBER: ret.size++; break;
            case MT_STRING: ret.size+=strlen(list.memory[i].string)+1; break;
        }
    }
    ret.cells = malloc(sizeof(MemoryCell) * ret.size);
    
    /* Step 2: zeroize label memory. */
    for (i=0; i<ret.size; ++i)
        ret.cells[i].labels = NULL;

    /* Step 3: begin filling cells. */
    index = 0;
    for (i=0; list.memory[i].type != MT_EOF; ++i) {
        ret.cells[index].labels = list.memory[i].labels;
        switch (list.memory[i].type) {
            case MT_INSTRUCTION:
                ret.cells[index].type = CT_INSTRUCTION;
                ret.cells[index].name = list.memory[i].name;
                ret.cells[index].shamt = list.memory[i].shamt;
                if (list.memory[i].r1.type != RT_NORMAL) 
                    ret.cells[index].r1 = 0xFE;
                else
                    ret.cells[index].r1 = list.memory[i].r1.number;
                if (list.memory[i].r2.type != RT_NORMAL) 
                    ret.cells[index].r2 = 0xFE;
                else
                    ret.cells[index].r2 = list.memory[i].r2.number;
                if (list.memory[i].r3.type != RT_NORMAL) 
                    ret.cells[index].r3 = 0xFE;
                else
                    ret.cells[index].r3 = list.memory[i].r3.number;
                index++;

                if (list.memory[i].r1.type == RT_LITERAL) {
                    ret.cells[index].type = CT_NUMBER;
                    ret.cells[index++].val = list.memory[i].r1.number;
                } else if (list.memory[i].r1.type == RT_LABEL) {
                    ret.cells[index].type = CT_REFERENCE;
                    ret.cells[index++].label = list.memory[i].r1.string;
                }
                if (list.memory[i].r2.type == RT_LITERAL) {
                    ret.cells[index].type = CT_NUMBER;
                    ret.cells[index++].val = list.memory[i].r2.number;
                } else if (list.memory[i].r2.type == RT_LABEL) {
                    ret.cells[index].type = CT_REFERENCE;
                    ret.cells[index++].label = list.memory[i].r2.string;
                }
                if (list.memory[i].r3.type == RT_LITERAL) {
                    ret.cells[index].type = CT_NUMBER;
                    ret.cells[index++].val = list.memory[i].r3.number;
                } else if (list.memory[i].r3.type == RT_LABEL) {
                    ret.cells[index].type = CT_REFERENCE;
                    ret.cells[index++].label = list.memory[i].r3.string;
                }
                break;
            case MT_NUMBER:
                ret.cells[index].type = CT_NUMBER;
                ret.cells[index++].val = list.memory[i].number;
                break;
            case MT_STRING:
                for (ii=0; list.memory[i].string[ii]; ++ii) {
                    ret.cells[index].type = CT_CHAR;
                    ret.cells[index++].ch = list.memory[i].string[ii];
                }
                ret.cells[index].type = CT_CHAR;
                ret.cells[index++].ch = '\0';
                free(list.memory[i].string);
                break;
        }
    }
    free(list.memory);

    return ret;
}



