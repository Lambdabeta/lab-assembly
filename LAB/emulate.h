#ifndef EMULATE_H
#define EMULATE_H

#include <stdio.h>

#define DIVIDE_BY_ZERO_ERROR 1
#define NEGATIVE_MEMORY_ACCESS_ERROR 2
#define OUT_OF_MEMORY_ERROR 3
#define INVALID_IO_ARGUMENT_EXCEPTION 4

typedef void *State;

State state_init(size_t);
void state_close(State);

State state_load(State,int*,size_t);
State state_loadf(State,FILE*);
int state_step(State);

int state_time(State);

#endif
