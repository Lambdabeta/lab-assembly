#include <stdio.h>
#include "emulate.h"

#define HELP_MESSAGE (\
        "LAB [executable]\n"\
        "\tRuns the specified executable file on a simulated LAB machine.")
#define FILE_NOT_FOUND_MESSAGE (\
        "Specified input file not found.")

#define DEFAULT_MEM_SIZE 10000
int main(int argc, char *argv[])
{
    if (argc!=2)
        return printf(HELP_MESSAGE),1;

    FILE *f = fopen(argv[1],"rb");
    if (!f) 
        return printf(FILE_NOT_FOUND_MESSAGE);
    
    //TODO: check for third argument and use it instead
    State s = state_init(DEFAULT_MEM_SIZE);

    state_loadf(s,f);

    while (!state_step(s));
    
    printf("\nExecution completed after %d cycles.\n",state_time(s));

    return 0;
}
