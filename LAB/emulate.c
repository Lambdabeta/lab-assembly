#include "emulate.h"
#include <stdlib.h> //for malloc

typedef struct StateTAG {
    int *mem;
    size_t limit;
    int time;

    int regs[256];
}*StateHANDLE;

State state_init(size_t memsize)
{
    int i;
    StateHANDLE ret = malloc(sizeof(struct StateTAG));
    ret->mem = malloc(sizeof(int)*memsize);
    ret->limit = memsize;
    ret->time = 0;
    
    for (i=0; i<256; ++i) ret->regs[i] = 0;
    ret->regs[253] = memsize-1;
    
    return ret;
}

State state_load(State s, int *data, size_t size)
{
    int i;
    StateHANDLE this = s;
    if (!this) goto state_load_ret;

    for (i=0; i<size; ++i) this->mem[i] = data[i];

state_load_ret:
    return this;
}

State state_loadf(State s, FILE *file)
{
    int i;
    StateHANDLE this = s;
    if (!this) goto state_loadf_ret;

    for (i=0; !feof(file); ++i) {
        unsigned char a,b,c,d;
        fread(&a,1,1,file);
        fread(&b,1,1,file);
        fread(&c,1,1,file);
        fread(&d,1,1,file);
        this->mem[i] = d | (c<<8) | (b<<16) | (a<<24);
    }
    fread(this->mem, sizeof(int), this->limit, file);

state_loadf_ret:
    return this;
}
    


void state_close(State s)
{
    StateHANDLE this = s;
    if (this) {
        if (this->limit > 0 && this->mem)
            free(this->mem);
        free(this);
    }
}


static int add(StateHANDLE this, int* a, int* b, int* c)
{
    *c = *a + *b;
    return 0;
}

static int sub(StateHANDLE this, int* a, int* b, int* c)
{
    *c = *a - *b;
    return 0;
}

static int mul(StateHANDLE this, int* a, int* b, int* c)
{
    *c = *a * *b;
    return 0;
}

/*underscore needed because stdlib has a div function already*/
static int div_(StateHANDLE this, int* a, int* b, int* c)
{
    if (!*b) return DIVIDE_BY_ZERO_ERROR;
    *c = *a / *b;
    return 0;
}

static int and(StateHANDLE this, int* a, int* b, int* c)
{
    *c = *a & *b;
    return 0;
}

/*underscore needed because stdlib has a nan function already */
static int nan_(StateHANDLE this, int* a, int* b, int* c)
{
    *c = ~(*a & *b);
    return 0;
}

static int orr(StateHANDLE this, int* a, int* b, int* c)
{
    *c = *a | *b;
    return 0;
}

static int nor(StateHANDLE this, int* a, int* b, int* c)
{
    *c = ~(*a | *b);
    return 0;
}

static int xor(StateHANDLE this, int* a, int* b, int* c)
{
    *c = *a ^ *b;
    return 0;
}

static int mod(StateHANDLE this, int* a, int* b, int* c)
{
    *c = *a % *b;
    return 0;
}

static int bgt(StateHANDLE this, int* a, int* b, int* c)
{
    if (*a > *b) this->regs[255] = *c;
    return 0;
}

static int beq(StateHANDLE this, int* a, int* b, int* c)
{
    if (*a == *b) this->regs[255] = *c;
    return 0;
}

static int ldr(StateHANDLE this, int* a, int* b, int* c)
{
    if (*a+*b < 0) return NEGATIVE_MEMORY_ACCESS_ERROR;
    if (*a+*b > this->limit) return OUT_OF_MEMORY_ERROR;
    *c = this->mem[*a+*b];
    return 0;
}

static int str(StateHANDLE this, int* a, int* b, int* c)
{
    if (*a+*b < 0) return NEGATIVE_MEMORY_ACCESS_ERROR;
    if (*a+*b > this->limit) return OUT_OF_MEMORY_ERROR;
    this->mem[*a+*b] = *c;
    return 0;
}

static int get(StateHANDLE this, int* a, int* b, int* c)
{
    int i;
    switch (*a) {
        case 0:
            *c = time(NULL) - *b;
            break;
        case 1:
            if (!*b) 
                scanf("%d",c);
            else
                scanf("%c",(char*)c);
            break;
        case 2: {
            char *buf = malloc(*b);
            fgets(buf,*b,stdin);
            for (i=0; i<*b; ++i) this->mem[*c+i] = buf[i];
                }
        default:
            return INVALID_IO_ARGUMENT_EXCEPTION;
    }
    return 0;
}

static int put(StateHANDLE this, int* a, int* b, int* c)
{
    int i;
    switch (*a) {
        case 0:
            if (!*b) return -1;
            break;
        case 1:
            if (!*b) {
                if (!*c) printf("0\n");
                else printf("%c\n",*c);
            } else printf("%d\n",*b);
            break;
        case 2:
            for (i=0; i<*b; ++i) {
                if (!this->mem[*c+i]) break;
                printf("%c",this->mem[*c+i]);
            }
            printf("\n");
            break;
        default:
            return INVALID_IO_ARGUMENT_EXCEPTION;
    }
    return 0;
}

typedef int (*instruction)(StateHANDLE, int*, int*, int*);
static instruction instructions[16] = {
    add,sub,mul,div_,and,nan_,orr,nor,xor,mod,bgt,beq,ldr,str,get,put
};
static int instruction_times[16] = {
    1,1,2,4,1,1,1,1,1,4,4,4,3,3,0,0
};

int state_step(State s)
{
    int ret;
    int inst;
    int shamt;
    int a,b,c;
    int *ar,*br,*cr;
    StateHANDLE this = s;

    if (!this) return 0;

    inst = this->mem[this->regs[255]++];

    c     =  inst & 0x000000FF;
    b     = (inst & 0x0000FF00) >> 8;
    a     = (inst & 0x00FF0000) >> 16;
    shamt = (inst & 0xF0000000) >> 28;
    inst  = (inst & 0x0F000000) >> 24; //Note: must be last because destructive

    if (a == 254) {
        ar = &(this->mem[this->regs[255]++]);
    } else {
        ar = &(this->regs[a]);
    }

    if (b == 254) {
        br = &(this->mem[this->regs[255]++]);
    } else {
        br = &(this->regs[b]);
    }
    
    if (c == 254) {
        cr = &(this->mem[this->regs[255]++]);
    } else {
        cr = &(this->regs[c]);
    }

    ret = instructions[inst](this,ar,br,cr);
    this->time += instruction_times[inst];

    switch (shamt) {
        case 0:          break; case 8:            break;
        case 1: *cr>>=1; break; case 9:  *cr>>=8;  break;//<<=32 becomes =0 to
        case 2: *cr>>=2; break; case 10: *cr>>=16; break;// avoid undefined beh.
        case 3: *cr>>=3; break; case 11: *cr>>=24; break;// since on 32-bit ints 
        case 4: *cr<<=4; break; case 12: *cr = 0;  break;// <<32 is undefined 
        case 5: *cr<<=3; break; case 13: *cr<<=24; break;
        case 6: *cr<<=2; break; case 14: *cr<<=16; break;
        case 7: *cr<<=1; break; case 15: *cr<<=8;  break;
    }
    
    return ret;
}
    
    
int state_time(State s)
{
    StateHANDLE this = s;
    if (!this) return 0;

    return this->time;
}
